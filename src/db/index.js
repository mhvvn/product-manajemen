import firebase from 'firebase'
import 'firebase/firestore'
const config = {
	apiKey: "AIzaSyDSbiamQvs3wV8EZ8P7qUlQLaECi0ZlQo8",
    authDomain: "produk-manajer.firebaseapp.com",
    databaseURL: "https://produk-manajer.firebaseio.com",
    projectId: "produk-manajer",
    storageBucket: "produk-manajer.appspot.com",
    messagingSenderId: "228862260075"
}
const firebaseapp = firebase.initializeApp(config)
export default firebaseapp.firestore()